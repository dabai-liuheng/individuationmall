package com.liuheng.indivimall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndivimallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndivimallWareApplication.class, args);
    }

}
