package com.liuheng.indivimall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndivimallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndivimallOrderApplication.class, args);
    }

}
