package com.liuheng.indivimall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndivimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndivimallProductApplication.class, args);
    }

}
