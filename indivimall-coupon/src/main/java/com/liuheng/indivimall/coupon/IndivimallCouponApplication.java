package com.liuheng.indivimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndivimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndivimallCouponApplication.class, args);
    }

}
