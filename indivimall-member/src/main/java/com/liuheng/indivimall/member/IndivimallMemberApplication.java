package com.liuheng.indivimall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndivimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndivimallMemberApplication.class, args);
    }

}
